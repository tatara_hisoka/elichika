# install the latest version of elichika from scratch
# set the environmental variable BRANCH to pick a specific branch (latest version only)
# this can be set with export or just set when invoking bash
# Honestly this is only for testing but you can think of it as a hidden feature
# if BRANCH is not provided, then default to main
# install git and golang
clear

echo -e "${C}${BOLD}please allow storage permission"${W}
while true; do
	termux-setup-storage
	sleep 4
    if [[ -d ~/storage ]]; then
        break
    else
        echo -e "${R}${BOLD}Storage permission denied\n"${W}
    fi
    sleep 2
done
echo "YOU ARE TRYING TO INSTALL DEVELOPEMENT VERSION, PLEASE TO REMIND YOU THAT THIS IS ONLY FOR REPORTING ISSUE"
echo ""
echo "Installing Elichika... PLEASE DO NOT DISCONNECT INTERNET"
echo "Download speed too slow? close installer by CTRL+C then use 1.1.1.1 and try again"
cd
rm -rf elichika2
pkg install golang git -y || echo "assuming go and git are already installed"
pkg install git -y || echo "assuming go and git are already installed"
pkg install python -y || echo "assuming go and git are already installed"
# clone the source code
git clone --depth 1 --branch developement --single-branch https://gitlab.com/tatara_hisoka/elichika.git elichika2 && \
cd elichika2 && \
# get the submodules (i.e. assets and other)
git submodule update --init && \
# build server, fallback to not using CGO to work on some devices
echo "Building executable, it takes 5 - 15 minutes+ depend your phone"
(go build || CGO_ENABLED=0 go build) && \
# set the permission
chmod +rx elichika && \
chmod +rx elichika_utility.sh && \
echo "Installed succesfully!"
if [ $? -eq 0 ]; then
    chmod +rwx ./bin/shortcut.sh && \
    ./bin/shortcut.sh
else
    echo "Error installing"
fi
package item

import (
	"elichika/client"
	"elichika/enum"
)

var (
	SchoolIdolRadianceR = client.Content{
		ContentType:   enum.ContentTypeGradeUpper,
		ContentId:     1800,
		ContentAmount: 1,
	}
	SchoolIdolRadianceSR = client.Content{
		ContentType:   enum.ContentTypeGradeUpper,
		ContentId:     1801,
		ContentAmount: 1,
	}
	SchoolIdolRadianceUR = client.Content{
		ContentType:   enum.ContentTypeGradeUpper,
		ContentId:     1802,
		ContentAmount: 1,
	}
	SchoolIdolSparkle = client.Content{
		ContentType:   enum.ContentTypeGradeUpper,
		ContentId:     1804,
		ContentAmount: 1,
	}
	SchoolIdolWish = client.Content{
		ContentType:   enum.ContentTypeGradeUpper,
		ContentId:     1805,
		ContentAmount: 1,
	}
)

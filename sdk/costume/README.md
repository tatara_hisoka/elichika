# About skin texture
credit to Yousifrill for drawing body texture

it always flat otherwise looks weird on flat

these recreation texture may not similar as official, if you want add your modified texture, let me know / make pull request

# Thigh Scale

## Nico Ruby Rina Lanzhu
- ScaleX 1.0
- ScaleY 0.941748
- ScaleZ 0.932695

## Nozomi Mari Karin Emma
- ScaleX 1.0
- ScaleY 1.03884
- ScaleZ 1.0577

# Skin Tone
- bright : Guilty Kiss, AZUNA, eli, nico, dia, ai, emma, shioriko, mia
- default : CYaRon, Printemp, umi, maki, kanan, hanamaru, kasumi, karin, kanata, rina
- slight : nozomi, lanzhu
- medium tone : rin

leave rina_unmask_costume_file empty if you not use chara_id 209

rina unmasked body also share same mesh as masked

you don't need thumbnail_file, it will use character thumbnail if is empty

on new version, chara_id variable is no longer neccessary but still used for old version mod

Create .txt file & copy this
```
costume_name_en = ""
costume_name_ko = ""
costume_name_zh = ""
costume_name_ja = ""
costume_description = ""

costume_file = "filename_with_extension" # example : tshirtcustom.101
rina_unmask_costume_file = ""
thumbnail_file = ""
```

# chara id list
- 1 - Honoka
- 2 - Eli
- 3 - Kotori
- 4 - Umi
- 5 - Rin
- 6 - Maki
- 7 - Nozomi
- 8 - Hanayo
- 9 - Nico
- 101 - Chika
- 102 - Riko
- 103 - Kanan
- 104 - Dia
- 105 - You
- 106 - Yohane
- 107 - Hanamaru
- 108 - Mari
- 109 - Ruby
- 201 - Ayumu
- 202 - Kasumi
- 203 - Shizuku
- 204 - Karin
- 205 - Ai
- 206 - Kanata
- 207 - Setsuna
- 208 - Emma
- 209 - Rina (require unmask costume)
- 210 - Shioriko
- 211 - Lanzhu
- 212 - Mia